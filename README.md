#Foobar-Editor#

##Setup##
Deploy the contents of the dist and test folder to a server and open index.html in your browser. To run the tests, open test.html in your browser.

##Features##
* Tabbed file editor
* Files are saved in DOM Storage
* Indication of modified and saved files
* Scales nicely to smaller screen sizes, works on mobile devices!
* Multiple users can use the same site, but only see their own files
* Built with [React](http://facebook.github.io/react/), [Boostrap](http://getbootstrap.com/) and [Jasmine](http://jasmine.github.io)

##Limitations##
* Does not run over https on all devices (some scripts are only served via http)
* The selected tab is not saved into persistent storage
* The tab order is not persisted
* The files are not synced for the same user between browsers
* jsx files are not precompiled
* The total file size is somewhat limited by the DOM storage limit, usually its 5MB or more, which is plenty for plain text.