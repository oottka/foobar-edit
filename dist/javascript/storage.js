(function(){

    var prefix = "com.oottka.foobar-editor";
    var fileSuffix = "file";
    var separator = "-";
    var fileKeyPrefix = prefix + separator + fileSuffix + separator;

    var saveOrUpdateFile = function(file){
        window.localStorage.setItem(createFileKey(file), JSON.stringify(file));
    };

    var deleteFile = function(file){
        window.localStorage.removeItem(createFileKey(file));
    };

    var getState = function(){
        var state = {};
        if(window.localStorage.length - 1){
            for(var i = 0; i < window.localStorage.length; i++){
                var key = window.localStorage.key(i);
                if(key.indexOf(fileKeyPrefix) > -1){
                    if(state.tabs == null){
                        state.tabs = [];
                    }
                    state.tabs.push({
                        file: JSON.parse(window.localStorage.getItem(key))
                    });
                }
            }
        }
        return state;
    };

    function createFileKey(file){
        return fileKeyPrefix + file.id;
    }

    if (typeof foobar == 'undefined'){
        foobar = {};
    }
    foobar.webstorage = {
        saveOrUpdateFile: saveOrUpdateFile
        ,deleteFile: deleteFile
        ,getState: getState
    }
})();