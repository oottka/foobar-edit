/** @jsx React.DOM */

var FileControl = React.createClass({
    handleChange: function() {
        this.props.onChange(
            this.refs.fileNameEditor.getDOMNode().value
        );
    },

    onClick: function(event){
        this.props.onSave(this.props.currentFile, event);
    },

    render: function() {
        return (
            <div className="file-control row">
                <div className="col-md-12">
                    <button
                        type="button"
                        disabled={this.props.currentFile == null ? 'disabled' : false}
                        className="btn btn-primary"
                        onClick={this.onClick}
                    >Save</button>
                    <input
                        type="text"
                        value={this.props.currentFile != null ? this.props.currentFile.name : ''}
                        disabled={this.props.currentFile == null ? 'disabled' : false}
                        ref="fileNameEditor"
                        onChange={this.handleChange}
                        maxLength="100"
                        className="filename"
                        />
                </div>
            </div>
        );
    }
});