/** @jsx React.DOM */

var NewFileTab = React.createClass({
    render: function() {
           return (
               <li className="tab newfile">
                   <a
                   href="#"
                   className="newfile-link"
                   onClick={this.props.onNewTabClick}
                   >
                       <div className="newfile-container">
                           <span className="glyphicon glyphicon-plus"></span>
                       </div>
                   </a>
               </li>
           );
       }
   });