/** @jsx React.DOM */

var FileTab = React.createClass({
    onClick: function(event) {
        this.props.onClick(this.props.tab, event);
    },

    onDeleteClick: function(event){
        this.props.onDeleteClick(this.props.tab, event);
    },

    render: function() {
        var cx = React.addons.classSet;
        var classes = cx({
            'tab': true
            ,'active': this.props.active
        });
        return (
            <li className={classes}>
                <a onClick={this.onClick} href="#">
                    <FileIcon modified={this.props.tab.file.modified} />
                    {this.props.tab.file.name}
                    <RemoveFileIcon onClick={this.onDeleteClick} />
                </a>
            </li>
        );
    }
});

var FileIcon = React.createClass({
    render: function() {
        var cx = React.addons.classSet;
        var classes = cx({
            'glyphicon': true
            ,'glyphicon-file': true
            ,dirty: this.props.modified
        });
        return <span className={classes}></span>
    }
});

var RemoveFileIcon = React.createClass({
    onClick: function(event) {
        this.props.onClick(event);
    },

    render: function() {
        return (
            <span
                className="glyphicon glyphicon-remove-circle"
                onClick={this.onClick}
            ></span>
            );
    }
});