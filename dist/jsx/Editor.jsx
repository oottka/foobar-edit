/** @jsx React.DOM */

var Editor = React.createClass({
    handleChange: function() {
        this.props.onChange(
            this.refs.fileContentEditor.getDOMNode().value
        );
    },

    render: function() {
        var cx = React.addons.classSet;
        var classes = cx({
            'textarea-wrapper': true
            ,'dirty': this.props.currentFile != null ? this.props.currentFile.modified : true
            ,'empty': this.props.currentFile == null
        });
        return (
            <div className="editor row">
                <div className="col-md-12">
                    <div className={classes}>
                        <textarea
                        id="editor-area"
                        cols="200"
                        rows="15"
                        value={this.props.currentFile != null ? this.props.currentFile.content : ''}
                        disabled={this.props.currentFile == null ? 'disabled' : false}
                        ref="fileContentEditor"
                        onChange={this.handleChange}>
                        </textarea>
                    </div>
                </div>
            </div>
        );
    }
});