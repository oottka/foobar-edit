/** @jsx React.DOM */

var Header = React.createClass({
    render: function() {
        return (
            <div className="header row">
                <div className="col-md-12">
                    <h1>Foobar-Editor</h1>
                </div>
            </div>
        );
    }
});