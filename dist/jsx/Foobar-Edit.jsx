/** @jsx React.DOM */

var PageContainer = React.createClass({
    createEmptyTab: function() {
        return  {
            file: {
               name: 'untitled'
               ,content: ''
               ,modified: true
               ,id: Math.random()
            }
        };
    },

    addNewTab: function(event) {
        event.preventDefault()
        var tabs = this.state.tabs;
        tabs.push(this.createEmptyTab());
        this.setState({
            tabs: tabs
            ,currentTabIndex: tabs.length - 1
        });
    },

    handleFileContentChange: function(text) {
        var tabs = this.state.tabs;
        tabs[this.state.currentTabIndex].file.content = text;
        tabs[this.state.currentTabIndex].file.modified = true;
        this.setState({tabs: tabs});
    },

    selectTab: function(clickedTab, event) {
        event.preventDefault();
        this.state.tabs.forEach(function(tab, index){
            if(tab.file.id == clickedTab.file.id){
                this.setState({currentTabIndex: index});
                return false;
            }
        }.bind(this));
    },

    renameFile: function(name) {
        var tabs = this.state.tabs;
        tabs[this.state.currentTabIndex].file.name = name;
        tabs[this.state.currentTabIndex].file.modified = true;
        this.setState({tabs: tabs});
    },

    saveFile: function(file, event) {
        event.preventDefault();
        var tabs = this.state.tabs;
        tabs[this.state.currentTabIndex].file.modified = false;
        foobar.webstorage.saveOrUpdateFile(tabs[this.state.currentTabIndex].file);
        this.setState({tabs: tabs});
    },

    deleteTab: function(tab, event) {
        event.preventDefault();
        event.stopPropagation();

        var tabs = this.state.tabs;
        var currentTabIndex = this.state.currentTabIndex;

        var index = tabs.indexOf(tab);

        /* If the deleted file was the only file, there is no current file.
         * Else decrement the index if needed. */
        if(tabs.length == 1){
            currentTabIndex = null;
        } else if(index <= currentTabIndex){
                        currentTabIndex--;
        }

        if(index > -1){
            foobar.webstorage.deleteFile(tabs[index].file);
            tabs.splice(index, 1);
        }
        this.setState({
            tabs: tabs
            ,currentTabIndex: currentTabIndex
        });
    },


    getInitialState: function() {
        var state = foobar.webstorage.getState();
        if (state.tabs == null){
            state.tabs = [this.createEmptyTab()];
        }
        state.currentTabIndex = 0;

        return state;
    },

    render: function() {
        var currentTab;
        if(this.state.currentTabIndex == null){
            currentTab = {file: null};
        } else {
            currentTab = this.state.tabs[this.state.currentTabIndex];
        }
        return (
        <div className="container">
            <Header />
            <TabBar
                tabs={this.state.tabs}
                currentTab={currentTab}
                addNewTab={this.addNewTab}
                selectTab={this.selectTab}
                deleteTab={this.deleteTab}
            />
            <Editor currentFile={currentTab.file} onChange={this.handleFileContentChange} />
            <FileControl
                currentFile={currentTab.file}
                onChange={this.renameFile}
                onSave={this.saveFile}
             />
            <BodyJavascripts />
        </div>
        );
    }
});

var BodyJavascripts = React.createClass({
    render: function() {
        return (
        <div>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
            <script src="javascript/bootstrap.min.js"></script>
        </div>
        );
    }
});

React.renderComponent(
  <PageContainer />,
  document.body
);