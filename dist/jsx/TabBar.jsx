/** @jsx React.DOM */

var TabBar = React.createClass({
    render: function() {
        var fileTabs = [];
        this.props.tabs.forEach(function(tab) {
            fileTabs.push(
                <FileTab
                    key={tab.file.id}
                    tab={tab}
                    active={tab == this.props.currentTab}
                    onClick={this.props.selectTab}
                    onDeleteClick={this.props.deleteTab}
                />
            );
        }.bind(this));

        return (
            <div className="tabs row">
                <div className="col-md-12">
                    <ul className="nav nav-pills tab-list" id="tab-list">
                        {fileTabs}
                        <NewFileTab onNewTabClick={this.props.addNewTab} />
                    </ul>
                </div>
            </div>
        );
    }
});