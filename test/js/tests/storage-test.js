describe("Storage layer", function(){
    var storage = foobar.webstorage;
    var file = validFile();

    function validFile(){
        return {
            name: 'untitled'
            ,content: ''
            ,modified: true
            ,id: Math.random()
        };
    };

    describe("When a file has been saved", function() {
        beforeEach(function(){
            storage.saveOrUpdateFile(file);
        });

        it("should be able get the file", function(){
            var state = storage.getState();
            expect(state.tabs).toContain({file: file});
        });

        afterEach(function(){
            storage.deleteFile(file);
        });
    });

    describe("When a file has been updated", function() {
        beforeEach(function(){
            storage.saveOrUpdateFile(file);
            file.name = 'bar';
            storage.saveOrUpdateFile(file);
        });

        it("we should be able to get the modified version", function(){
            var state = storage.getState();
            expect(state.tabs).toContain({file: file});
        });

        afterEach(function(){
            storage.deleteFile(file);
        });
    });

    describe("When a file has been deleted", function() {
        beforeEach(function(){
            storage.saveOrUpdateFile(file);
            storage.deleteFile(file);
        });

        it("we should no longer be able to retrieve the file", function(){
            var state = storage.getState();
            expect(state.tabs).not.toContain({file: file});
        })
    })
});